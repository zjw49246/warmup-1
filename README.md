# Warmup 1

Complete this assignment by completing the EXERCISES in `src/lib.rs`.

Submit this to gradescope either by submitting a *private* git repo on
GitHub or Bitbucket or by submitting a zip file containing (at least)
the following files:

1. `Cargo.toml`
2. `src/lib.rs`

If you get a compilation error, make sure that your zip file contains
the files themselves, not the directory containing them. That is,
running `unzip submission.zip` in an empty directory `tmp` should
add `Cargo.toml` and `src/lib.rs` directly to `tmp`.
