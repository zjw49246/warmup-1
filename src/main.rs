/* This is the main file, which you can use for playing around with Rust.
 *
 * This file will not be graded, (and doesn't need to be included with your submission)
 * but make sure if you do include it that it compiles.
 */
use warmup1::*; // This imports everything from lib.rs

fn main() {
    println!("This is assignment {}", foo(1));
}
