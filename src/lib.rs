
/* This is the starter code for warmup 1.
 *
 * Fill in the stub functions with your implementation, and fill in
 * the comments with your responses to the questions.
 */


/* Here is an example function. Note the "pub" here means that you are
 * exporting this function so other Rust programs (like main.rs or the
 * autograder) can use them */
pub fn foo(x: i32) -> i32 {
    x
}

/* EXERCISE 1a: Implement a function that returns the ith fibonacci number, starting with 0, 1
 * https://en.wikipedia.org/wiki/Fibonacci_number
 */
pub fn fibonacci(i: u32) -> u32 {
    panic!("NYI: fibonacci")
}


#[cfg(test)]
mod fib_tests {
    use super::*; // This imports all functions from the top-level module of this file.
    #[test]
    fn fib_test_0() {
        assert_eq!(fibonacci(0), 0);
    }
    #[test]
    fn fib_test_1() {
        assert_eq!(fibonacci(1), 1);
    }

    /* EXERCISE 1b: Make sure to test your fibonacci function */
}

/* EXERCISE 2 */
/* Return the string slice with the smallest length of those passed in
 * If none are passed in, you should use panic!()
 * If multiple have the same length, return the one occurring earliest in the input
 */
pub fn smallest_str<'a, 'b>(strings: &'b[&'a str]) -> &'a str {
    panic!("NYI: smallest_str")
}

#[cfg(test)]
mod smallest_string_tests {
    use super::*; // This imports all functions from the top-level module of this file.
    #[test]
    fn ss_test_1() {
        assert_eq!(smallest_str(&vec!["hi","there"]), "hi");
    }

    /* EXERCISE 2b: Make sure to test your functions!
     * Test corner cases especially!
     */
}

/* EXERCISE 3: Write your response in a comment below this
 * Look at the type of smallest_str
 *
 *   smallest_str<'a, 'b>(strings: &'b[&'a str]) -> &'a str
 *
 * Consider the following alternative lifetime annotations for the same function:
 *
 *   1. smallest_str<'a>(strings: &'a[&'a str]) -> &'a str
 *   2. smallest_str<'a, 'b>(strings: &'a[&'a str]) -> &'b str
 *   3. smallest_str<'a, 'b>(strings: &'b[&'a str]) -> &'b str
 * 
 * Why are these type signatures less useful than the original? Which of the 3
 * are even implementable?
 * 
 * Now consider some other choices of whether or not inputs or outputs are borrowed:
 *   1. smallest_str(strings: Vec<String>) -> String
 *   2. smallest_str(strings: Vec<&str>) -> &str
 * 
 * Why might we prefer the original type to each of these types? 
 */


/* EXERCISE 4
 * Implement a function that takes in a vector of strings and outputs
 * a vector where the strings have all been concatenated with
 * themselves.
 * 
 * Useful methods from the stdlib are "clone" and "push_str"
 */
pub fn blah_blah(strings: &[String]) -> Vec<String> {
    panic!("NYI: blah_blah")
}

#[cfg(test)]
mod blah_blah_tests {
    use super::*;
    #[test]
    fn blah_blah_1() {
        assert_eq!(blah_blah(&vec![String::from("blah"), String::from("yada_")]),
		   vec![String::from("blahblah"), String::from("yada_yada_")]);
    }

    /* EXERCISE 4b: Make sure to test your functions!
     */
}

/* EXERCISE 5
 * Implement a function that adds the 2nd and 4th elements of a slice together (0 indexed of course).
 * Return None if the vector doesn't have enough elements instead of using panic!
 * 
 * To get full credit, don't directly use array indexing, instead use
 * the `get` method:
 * https://doc.rust-lang.org/std/primitive.slice.html#method.get on
 * slices to get an Option and either use pattern matching or the `?`
 * operator to implement the correct behavior.
 */
pub fn add_them_up(v: &[i32]) -> Option<i32> {
    panic!("NYI: add_them_up")
}

#[cfg(test)]
mod add_them_up_tests {
    use super::*;
    #[test]
    fn add_them_up_1() {
        assert_eq!(add_them_up(&vec![1,2,4,8,16,32]), Some(20));
    }

    /* EXERCISE 4b: Make sure to test your functions!
     */
}
